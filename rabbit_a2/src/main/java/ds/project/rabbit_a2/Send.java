package ds.project.rabbit_a2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import ds.project.rabbit_a2.model.MonitoredData;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableScheduling
@SpringBootApplication
public class Send{
    private final static String QUEUE_NAME = "hello";



    public static void main(String[] args) throws Exception {

        //Read file and put it in stream
        String fileName = new File(".").getCanonicalPath() + "\\activity.txt";
        List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            monitoredData = stream.map(a -> a.split("\\s\\s+"))
                    .map(arr -> new MonitoredData(Integer.parseInt(arr[0]),  arr[3].trim(), arr[1], arr[2])).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //----------------Take 2
//        SpringApplication.run(Send.class, args);

        //----------------- Works on strings -------------------------------------
        for (MonitoredData m : monitoredData) {
            sendMessage(m);
            System.out.println(" [x] Sent '" + m + "'");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException _ignored) {
                Thread.currentThread().interrupt();
            }

        }
//            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
        //----------------------------------------------------------------------------------------

    }

    //-------------------------------Varianta care merge pe stringuri--------------------------------------------------
    @Scheduled(fixedDelay = 1000L)
    public static void sendMessage(MonitoredData message) throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
//        factory.setPort(5676);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

//            JSONObject obj = message.toString();

//            byte[] data = SerializationUtils.serialize(message);
            channel.basicPublish("", QUEUE_NAME, null, message.toStringJSON().getBytes());

        }
    }


    //-------------------------------------------------------------------------------------------------------------------
    //TAKE 2

//    @Autowired
//    Producer producer;
//
//    @Override
//    public void run(String... args) throws Exception {
//
//        //Read file and put it in stream
//        String fileName = new File(".").getCanonicalPath() + "\\activity.txt";
//        List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
//        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
//            monitoredData = stream.map(a -> a.split("\\s\\s+"))
//                    .map(arr -> new MonitoredData(1,  arr[2], toDate(arr[0]), toDate(arr[1]))).collect(Collectors.toList());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        for (MonitoredData m : monitoredData) {
////            sendMessage(m);
//            producer.produce(m);
//            System.out.println(" [x] Sent '" + m + "'");
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException _ignored) {
//                Thread.currentThread().interrupt();
//            }
//        }
//    }



    //-------------------------------------------------------------------------------------------------------------------


    //-----------------Asta imi trebe sa convertesc String Date to Date--------------------------------
    private static Date toDate(String d) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date data = null;
        try {
            data = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return data;
    }
}