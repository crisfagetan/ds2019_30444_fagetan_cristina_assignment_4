package ds.project.assignment1.server;

import assign3.grpc.*;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.entities.MedicationPlan;
import ds.project.assignment1.entities.Patient;
import ds.project.assignment1.repositories.MedPlanRepository;
import ds.project.assignment1.repositories.PatientRepository;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class HelloServiceImpl extends PillDispenserServiceGrpc.PillDispenserServiceImplBase {

    MedPlanRepository repo;
    PatientRepository patientRepository;

    @Autowired
    HelloServiceImpl(MedPlanRepository repo, PatientRepository patientRepository){
        this.repo = repo;
        this.patientRepository = patientRepository;
    }

    @Override
    public void download(
            DownloadRequest request, StreamObserver<GrpcMedPlan> responseObserver) {
        System.out.println("Request received from client:\n" + request);

        int patientId = request.getPatientId();

        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientId, false);

        List<MedicationPlan> meds = repo.findAllByPatientAndIsRemoved(patient, false);


        List<GrpcMedication> grpcMedPlans = new ArrayList<>();

        for(MedicationPlan m: meds){

            List<GrpcDrug> grpcDrugs = new ArrayList<>();
            for(Drug drug: m.getDrugs()){
                GrpcDrug d = GrpcDrug.newBuilder()
                        .setDrugId(drug.getId())
                        .setName(drug.getName())
                        .build();
                grpcDrugs.add(d);
            }

            GrpcMedication grpcMedPlan = GrpcMedication.newBuilder()
                    .setMedId(m.getId())
                    .addAllDrug(grpcDrugs)
                    .setIntakeCount(m.getIntakeCount())
                    .setIntakeInterval(m.getIntakeInterval())
                    .setStartHour(m.getStartHour().toString()).build();
            grpcMedPlans.add(grpcMedPlan);

        }

        GrpcMedPlan.Builder builder = GrpcMedPlan.newBuilder();

        for(GrpcMedication m: grpcMedPlans){
            builder.addMedication(m);
        }

        GrpcMedPlan response = builder.build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void taken(GrpcTaken request,
                      StreamObserver<Empty> responseObserver) {
        int medId = request.getMedPlanId();
        System.out.println("Medicine from was taken");
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
    @Override
    public void notTaken(GrpcTaken request,
                      StreamObserver<Empty> responseObserver) {
        int medId = request.getMedPlanId();
        System.out.println("Medicine from MedID "+ medId+" NOT TAKEN");
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
}
