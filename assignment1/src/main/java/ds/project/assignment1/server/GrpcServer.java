package ds.project.assignment1.server;

import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GrpcServer {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(GrpcServer.class);
        HelloServiceImpl service;

        @Autowired
        GrpcServer(HelloServiceImpl service){
            this.service = service;
            try{
               ServerBuilder.forPort(6565).addService(service).build().start().awaitTermination();

            }
            catch(Exception e){

            }


        }

}
