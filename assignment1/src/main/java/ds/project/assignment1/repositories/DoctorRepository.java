package ds.project.assignment1.repositories;

import ds.project.assignment1.entities.Doctor;
import ds.project.assignment1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {


     public Doctor findDoctorByDoctorUser(User userId);
     public Doctor findDoctorByIdAndIsRemoved(int id, Boolean isRemoved);
     public List<Doctor> findAllByIsRemoved(Boolean isRemoved);

}

