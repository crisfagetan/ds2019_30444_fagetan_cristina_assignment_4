package ds.project.assignment1.repositories;

import ds.project.assignment1.entities.MonitoredData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoredDataRepository extends JpaRepository<MonitoredData, Integer> {
}
