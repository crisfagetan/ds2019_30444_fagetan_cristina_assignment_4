package ds.project.assignment1.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "drugs")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Drug {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "side_effects")
    private String sideEffects;

    @Column(name = "dosage")
    private Integer dosage;

    @JsonIgnore
    @ManyToMany(mappedBy = "drugs", fetch = FetchType.LAZY)
    private List<MedicationPlan> medPlans;

    @Column(name = "is_removed")
    private Boolean isRemoved;

    //------------------------------------------------- CONSTRUCTORS ----------------------------------------------------------------------


    //Initializer constructor for DTO
    public Drug(Integer id, String name, String sideEffects, Integer dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        medPlans = new ArrayList<>();
        this.isRemoved = false;
    }

    //------------------------------------------------- GET & SET ----------------------------------------------------------------------

}
