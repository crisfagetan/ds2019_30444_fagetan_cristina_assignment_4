package ds.project.assignment1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregivers")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Caregiver{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JsonIgnore
    @JoinColumn( name = "user_id")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User caregiverUser;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "Caregiver_Patients",
            joinColumns = { @JoinColumn(name = "caregiver_id") },
            inverseJoinColumns = { @JoinColumn(name = "patient_id") }
    )
    List<Patient> patients;

    @Column(name = "is_removed")
    private Boolean isRemoved;
    //------------------------------------------------- CONSTRUCTORS ----------------------------------------------------------------------

    public Caregiver(User user){
        this.caregiverUser = user;
        this.id = 0;
        this.patients = new ArrayList<Patient>();
        this.isRemoved = false;
    }

    //--------------------------------------------- GET & SET -------------------------------------------------------------------------

}
