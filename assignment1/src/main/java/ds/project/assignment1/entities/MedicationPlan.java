package ds.project.assignment1.entities;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "drugs_in_medplan",
            joinColumns = { @JoinColumn(name = "medication_id") },
            inverseJoinColumns = { @JoinColumn(name = "drug_id") }
    )
    List<Drug> drugs;

    //The maximum number it can take in one day
    @Column(name = "intake_count")
    private Integer intakeCount;

    //once every <intakeInterval> hours
    @Column(name = "intake_interval")
    private Integer intakeInterval;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="HH:mm")
    @Column(name = "start_hour")
    private LocalTime startHour;

    //Period of treatment
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    @Column(name = "start_date")
    private LocalDate startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "is_removed")
    private Boolean isRemoved;

    @Column(name ="is_taken")
    private Boolean isTaken;

    //-------------------------------------------------CONSTRUCTORS-----------------------------------------------------------------------

    public MedicationPlan(Integer id, Doctor doctor, List<Drug> drugs, Integer intakeCount, Integer intakeInterval, LocalTime startHour, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.doctor = doctor;
     //   this.patient = patient;
        this.drugs = drugs;
        this.intakeCount = intakeCount;
        this.intakeInterval = intakeInterval;
        this.startHour = startHour;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isRemoved = false;
    }


    //--------------------------------------------GET & SET-------------------------------------------------------------



}
