package ds.project.assignment1.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.project.assignment1.entities.Caregiver;
import ds.project.assignment1.entities.Patient;
import ds.project.assignment1.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaregiverViewDTO {

    private Integer id;

    private Integer userId;

    private String firstName;

    private String lastName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date dateOfBirth;

    private Boolean gender; //1 = male; 0 = female

    private String address;

    List<Patient> patients;


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getCaregiverUser().getId(),
                caregiver.getCaregiverUser().getFirstName(),
                caregiver.getCaregiverUser().getLastName(),
                caregiver.getCaregiverUser().getDateOfBirth(),
                caregiver.getCaregiverUser().getGender(),
                caregiver.getCaregiverUser().getAddress(),
                caregiver.getPatients());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO forViewDTO, User user){
            user.setAddress(forViewDTO.getAddress());
            user.setFirstName(forViewDTO.getFirstName());
            user.setLastName(forViewDTO.getLastName());


        return new Caregiver(
                forViewDTO.getId(),
                user,
                forViewDTO.getPatients(),
                false);
    }
}
