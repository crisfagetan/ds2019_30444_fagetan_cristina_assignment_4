package ds.project.assignment1.services;

import ds.project.assignment1.dtos.DrugDTO;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.errorhandlers.ResourceNotFoundException;
import ds.project.assignment1.repositories.DrugRepository;
import ds.project.assignment1.validators.DrugFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DrugService {

    private final DrugRepository drugRepository;

    @Autowired
    public DrugService(DrugRepository drugRepository) {
        this.drugRepository = drugRepository;
    }

    //-------------------------------------------- FIND ----------------------------------------------------------------

    public List<DrugDTO> findAll(){
        List<Drug> drugs = drugRepository.getAllOrdered();

        return drugs.stream()
                .map(DrugDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public DrugDTO findDrugById(Integer id){
        Drug drug  = drugRepository.findDrugByIdAndIsRemoved(id, false);

        if (drug == null) {
            throw new ResourceNotFoundException("Drug", "user id", id);
        }
        return DrugDTO.generateDTOFromEntity(drug);
    }

    public Drug findDrugById2(Integer id){
        Drug drug  = drugRepository.findDrugByIdAndIsRemoved(id, false);

        if (drug == null) {
            throw new ResourceNotFoundException("Drug", "user id", id);
        }
        return drug;
    }

    //-------------------------------------------- CREATE ----------------------------------------------------------------

    public Integer insert(DrugDTO drugDTO) {

        DrugFieldValidator.validateInsertOrUpdate(drugDTO);

        Drug drug = DrugDTO.generateEntityFromDTO(drugDTO);
        drug.setIsRemoved(false);
        return drugRepository
                .save(drug)
                .getId();
    }

    //-------------------------------------------- UPDATE ----------------------------------------------------------------

    public Integer update(DrugDTO drugDTO) {

        Drug drug = drugRepository.findDrugByIdAndIsRemoved(drugDTO.getId(), false);

        if(drug == null){
            throw new ResourceNotFoundException("MedPlan", "MedPlan id", drugDTO.getId().toString());
        }
        DrugFieldValidator.validateInsertOrUpdate(drugDTO);

        return drugRepository.save(DrugDTO.generateEntityFromDTO(drugDTO)).getId();
    }

    //-------------------------------------------- DELETE ----------------------------------------------------------------

    public void delete(DrugDTO drugDTO){
        Drug drug = drugRepository.findDrugByIdAndIsRemoved(drugDTO.getId(), false);
        drug.setIsRemoved(true);
        drugRepository.save(drug);
    }





}
