package ds.project.assignment1.services;


import ds.project.assignment1.dtos.MedPlanToAddDTO;
import ds.project.assignment1.dtos.MedPlanViewDTO;
import ds.project.assignment1.entities.Doctor;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.entities.MedicationPlan;
import ds.project.assignment1.entities.Patient;
import ds.project.assignment1.errorhandlers.ResourceNotFoundException;
import ds.project.assignment1.repositories.DoctorRepository;
import ds.project.assignment1.repositories.DrugRepository;
import ds.project.assignment1.repositories.MedPlanRepository;
import ds.project.assignment1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedPlanService {

    private final MedPlanRepository medPlanRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final DrugRepository drugRepository;

    @Autowired
    public MedPlanService(MedPlanRepository medPlanRepository, DrugRepository drugRepository, DoctorRepository doctorRepository, PatientRepository patientRepository) {
        this.medPlanRepository = medPlanRepository;
        this.doctorRepository = doctorRepository;
        this.drugRepository = drugRepository;
        this.patientRepository = patientRepository;

    }

    //------------------------------------------ FIND -------------------------------------------------------------------

    public List<MedPlanViewDTO> findAll(){
        List<MedicationPlan> medicationPlans = medPlanRepository.findAllByIsRemoved(false);

        return medicationPlans.stream()
                .map(MedPlanViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public MedPlanViewDTO findUserById(Integer id){
        MedicationPlan medicationPlan = medPlanRepository.findMedicationPlanByIdAndIsRemoved(id, false);

        if (medicationPlan == null) {
            throw new ResourceNotFoundException("Medication Plan", "medPlan id", id);
        }

        return MedPlanViewDTO.generateDTOFromEntity(medicationPlan);
    }


    //------------------------------------------------ CREATE ----------------------------------------------------------

    public Integer insert(MedPlanToAddDTO medPlanToAddDTO, Integer doctorId, Integer patientId) {

        //DrugFieldValidator.validateInsertOrUpdate(medPlanToAddDTO);

        Patient patient = patientRepository.findPatientByIdAndIsRemoved(patientId, false);
        Doctor doctor = doctorRepository.findDoctorByIdAndIsRemoved(doctorId, false);
        List<Drug> drugs = new ArrayList<>();
        for (Drug d: medPlanToAddDTO.getDrugs()) {
            drugs.add(drugRepository.findDrugByIdAndIsRemoved(d.getId(), false));
        }

        MedicationPlan medicationPlan = MedPlanToAddDTO.generateEntityFromDTO(medPlanToAddDTO, doctor, patient);
        medicationPlan.setIsRemoved(false);
        medicationPlan.setDrugs(drugs);

        return medPlanRepository
                .save(medicationPlan)
                .getId();
    }


    //------------------------------------------------- UPDATE ---------------------------------------------------------


    public Integer update(MedPlanViewDTO medPlanViewDTO) {

        MedicationPlan medicationPlan = medPlanRepository.findMedicationPlanByIdAndIsRemoved(medPlanViewDTO.getId(), false);

        if(medicationPlan == null){
            throw new ResourceNotFoundException("Medication Plan", "medPlan id", medPlanViewDTO.getId().toString());
        }
        //TODO
       // UserFieldValidator.validateRegister(medPlanViewDTO);

        return medPlanRepository.save(MedPlanViewDTO.generateEntityFromDTO(medPlanViewDTO)).getId();
    }

    //------------------------------------------------ DELETE ----------------------------------------------------------

    public void delete(MedPlanViewDTO medPlanViewDTO){
        MedicationPlan medicationPlan = medPlanRepository.findMedicationPlanByIdAndIsRemoved(medPlanViewDTO.getId(), false);
        medicationPlan.setIsRemoved(true);
        medPlanRepository.save(medicationPlan);
    }




}
