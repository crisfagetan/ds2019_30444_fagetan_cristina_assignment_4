package ds.project.assignment4_doctor_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment4DoctorProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment4DoctorProducerApplication.class, args);
    }

}
