package ds.project.assignment4_doctor_producer.endpoints;

import doctorapp.doctor.*;
import ds.project.assignment4_doctor_producer.entities.MonitoredDataEntity;
import ds.project.assignment4_doctor_producer.entities.UserEntity;
import ds.project.assignment4_doctor_producer.repositories.IMonitoredDataRepository;
import ds.project.assignment4_doctor_producer.repositories.IUserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Endpoint
public class MonitoredDataEndpoint {

    private static final String NAMESPACE_URI = "http://doctorApp/doctor";

    private IMonitoredDataRepository monitoredDataRepo;

    @Autowired
    public MonitoredDataEndpoint(IMonitoredDataRepository monitoredDataRepository) {
        this.monitoredDataRepo = monitoredDataRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivities(@RequestPayload GetActivityRequest request) {
        GetActivityResponse response = new GetActivityResponse();
        List<MonitoredDataEntity> monitoredData = monitoredDataRepo.findAllByPatientId(request.getPatientId());
        List<MonitoredData> monitoredDataXSD = new ArrayList<>();

        for (MonitoredDataEntity md:monitoredData) {
            MonitoredData m = new MonitoredData();
            m.setActivity(md.getActivity());
            m.setEndTime(md.getEndTime().toString());
            m.setId(md.getId());
            m.setPatientId(md.getPatientId());
            m.setStartTime(md.getStartTime().toString());
            m.setStatus(md.getStatus());
            monitoredDataXSD.add(m);
        }
        response.getActivity().addAll(monitoredDataXSD);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "changeActivityRequest")
    @ResponsePayload
    public ChangeActivityResponse changeActivities(@RequestPayload ChangeActivityRequest request) {
        ChangeActivityResponse response = new ChangeActivityResponse();
        MonitoredDataEntity monitoredData = monitoredDataRepo.findMonitoredDataEntitiesById(request.getActivity().getId());

        if(request.getActivity().getStatus() != ""){
            monitoredData.setStatus(request.getActivity().getStatus());
        }

        if(request.getActivity().getRecommend() != ""){
            monitoredData.setActivity(request.getActivity().getRecommend());
        }

        monitoredDataRepo.save(monitoredData);

        response.setChanged(1);

        return response;
    }
}
