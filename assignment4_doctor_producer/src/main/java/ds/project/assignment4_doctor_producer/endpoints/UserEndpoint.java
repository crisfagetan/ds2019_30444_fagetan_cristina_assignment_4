package ds.project.assignment4_doctor_producer.endpoints;


import doctorapp.doctor.GetLoginRequest;
import doctorapp.doctor.GetLoginResponse;
import doctorapp.doctor.User;
import ds.project.assignment4_doctor_producer.entities.UserEntity;
import ds.project.assignment4_doctor_producer.repositories.IUserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class UserEndpoint {

    private static final String NAMESPACE_URI = "http://doctorApp/doctor";

    private IUserRepository userRepository;

    @Autowired
    public UserEndpoint(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetLoginResponse getLogin(@RequestPayload GetLoginRequest request) {
        GetLoginResponse response = new GetLoginResponse();
        UserEntity u = userRepository.findUserByUsernameAndPassword(request.getUsername(), request.getPassword());
        User u2 = new User();
        BeanUtils.copyProperties(u, u2);
        response.setUser(u2);

        return response;
    }
}
