package ds.project.assignment4_doctor_producer.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "monitored_data")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class MonitoredDataEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    int id;

    @Column(name = "patient_id")
    int patientId;

    @Column(name = "start_time")
    Date startTime;

    @Column(name = "end_time")
    Date endTime;

    @Column(name = "activity")
    String activity;

    @Column(name= "status")
    String status;

    @Column(name= "recommend")
    String recommend;

    public MonitoredDataEntity(int patientId, String activity, Date startTime, Date endTime, String recommend, String status) {
        this.patientId = patientId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        this.recommend = recommend;
        this.status = status;
    }

    public MonitoredDataEntity() {
        super();
        // TODO Auto-generated constructor stub
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getPatientId() {
//        return patientId;
//    }
//
//    public void setPatientId(int patientId) {
//        this.patientId = patientId;
//    }
//
//    public String getActivity() {
//        return activity;
//    }
//
//    public void setActivity(String activity) {
//        this.activity = activity;
//    }
//
//    public Date getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(Date startTime) {
//        this.startTime = startTime;
//    }
//
//    public Date getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(Date endTime) {
//        this.endTime = endTime;
//    }

    public String dateToString(Date d) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return format.format(d);
    }

    @Override
    public String toString() {
        return "Patient_id: " + patientId +
                " Start time: " + dateToString(getStartTime()) +
                " End time: " + dateToString(getEndTime()) +
                " Activity: " + getActivity();

    }

    /**
     *
     * Show Detail View
     */
//    public String toStringJSON(){
//        JSONObject jsonInfo = new JSONObject();
//
//        try {
//            jsonInfo.put("patient_id", this.patientId);
//            jsonInfo.put("activity", this.activity);
//            jsonInfo.put("start", this.startTime);
//            jsonInfo.put("end", this.endTime);
//        } catch (JSONException e1) {}
//        return jsonInfo.toString();
//    }
}
