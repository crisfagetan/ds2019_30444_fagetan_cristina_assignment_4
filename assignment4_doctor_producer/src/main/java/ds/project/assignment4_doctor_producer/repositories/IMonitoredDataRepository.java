package ds.project.assignment4_doctor_producer.repositories;

import doctorapp.doctor.MonitoredData;
import ds.project.assignment4_doctor_producer.entities.MonitoredDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IMonitoredDataRepository extends JpaRepository<MonitoredDataEntity, Integer> {

    public List<MonitoredDataEntity> findAllByPatientId(int patientId);
    public MonitoredDataEntity findMonitoredDataEntitiesById(int id);
}
