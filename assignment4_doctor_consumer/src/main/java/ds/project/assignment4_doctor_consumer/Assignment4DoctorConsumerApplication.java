package ds.project.assignment4_doctor_consumer;

//import com.example.consumingwebservice.wsdl.GetActivityResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PostConstruct;
import javax.print.Doc;

@SpringBootApplication
public class Assignment4DoctorConsumerApplication {

    @Autowired
    private ObjectMapper objectMapper;

    public static void main(String[] args) {

        SpringApplication.run(Assignment4DoctorConsumerApplication.class, args);

//        LoginFrame start = new LoginFrame();
//        start.setVisible(true);
    }


    //Add CORS so that the browser trusts the server
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:8083");
            }
        };
    }

//    @Bean
//    CommandLineRunner lookup(UserClient quoteClient) {
//        return args -> {
//            String username = "caregiver1";
//
//            if (args.length > 0) {
//                username = args[0];
//            }
//            GetCountryResponse response = quoteClient.getCountry(username);
//            System.err.println(response.getUser().getFirstName());
//        };
//    }

//    @Bean
//    int lookup(DoctorClient quoteClient) {
//      //  return args -> {
//            int id = 1;
//
////            if (args.length > 0) {
////                id = Integer.parseInt(args[0]);
////            }
//            GetActivityResponse response = quoteClient.getActivities(id);
//            System.err.println(response.getActivity().get(0).getActivity());
//            return 0;
//       // };
//    }

//    @Bean
//    User lookup(DoctorClient client) {
//        //  return args -> {
//        int id = 1;
//
////            if (args.length > 0) {
////                id = Integer.parseInt(args[0]);
////            }
//        try {
//
//            GetLoginResponse response = client.getLogin("caregiver1", "pass");
//            System.err.println(response.getUser().getFirstName());
//            return response.getUser();
//        }
//        catch(Exception e){
//            e.printStackTrace();
//        }
//        return null;
//        // };
//    }
}
