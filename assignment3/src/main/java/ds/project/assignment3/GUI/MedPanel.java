package ds.project.assignment3.GUI;

import assign3.grpc.GrpcTaken;
import assign3.grpc.PillDispenserServiceGrpc;
import ds.project.assignment3.model.DrugInMed;
import ds.project.assignment3.model.MedicationPlan;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;


public class MedPanel extends JPanel {

    private JTable table;
    DefaultTableModel model;
    private JLabel MedPlanId = new JLabel("ID:");
    private JLabel IntakeInterval = new JLabel("Intake Interval:");
    private JLabel IntakeCount = new JLabel("Intake count: ");
    private JLabel StartHour = new JLabel("Start Hour: ");

    private JLabel medI = new JLabel();
    private JLabel ii =  new JLabel();
    private JLabel ic =  new JLabel();
    private JLabel sh=  new JLabel();
    public ButtonColumn buttonColumn;


    public MedPanel(MedicationPlan med){

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JPanel newPanel = new JPanel();


        medI.setText(med.getId().toString());
        MedPlanId.setLabelFor(medI);
        ii.setText(med.getIntakeInterval().toString());
        IntakeInterval.setLabelFor(ii);
        ic.setText(med.getIntakeCount().toString());
        IntakeCount.setLabelFor(ic);
        sh.setText(med.getStartHour().toString());
        StartHour.setLabelFor(sh);
        newPanel.add(MedPlanId);
        newPanel.add(medI);
        newPanel.add(IntakeInterval);
        newPanel.add(ii);
        newPanel.add(IntakeCount);
        newPanel.add(ic);
        newPanel.add(StartHour);
        newPanel.add(sh);



        String data[][] = new String[10][2];

        int i=0;
        for(DrugInMed medName: med.getDrug()){
            data[i][0]= String.valueOf(medName.getName());
            data[i][1]= "TAKEN";
            i++;
        }
        String columnNames[] = {"NAME", ""};
        model = new DefaultTableModel(data, columnNames);
        table = new JTable( model );
//        JTable jt = new JTable(data, columnNames);
//        JScrollPane sp = new JScrollPane(jt, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
//                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        Action takenMedicine = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                 table = (JTable)e.getSource();
                int modelRow = Integer.valueOf( e.getActionCommand() );
                ((DefaultTableModel)table.getModel()).removeRow(modelRow);
                notifyTaken();
            }
        };

        table.setShowVerticalLines(false);
        buttonColumn = new ButtonColumn(table, takenMedicine, 1);
        buttonColumn.setMnemonic(KeyEvent.VK_D);
        JScrollPane scrollPane = new JScrollPane(table);
        this.add(newPanel);
        this.add(scrollPane);

        this.setBorder(BorderFactory.createLineBorder(Color.green));
    }

    private void notifyTaken(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();
        PillDispenserServiceGrpc.PillDispenserServiceBlockingStub stub = PillDispenserServiceGrpc.newBlockingStub(channel);

        stub.taken(GrpcTaken.newBuilder()
                .setDrugId(1)
                .setMedPlanId(1)
                .build());

        channel.shutdown();
    }


}
