package ds.project.assignment3.GUI;


import assign3.grpc.GrpcTaken;
import assign3.grpc.PillDispenserServiceGrpc;
import ds.project.assignment3.model.MedicationPlan;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PillBoxFrame extends JFrame{
     JLabel title = new JLabel("PILLBOX");
     JPanel topPanel = new JPanel();
     JLabel timer = new JLabel();
     JPanel nextPanel = new JPanel();
     private JTable table = new JTable();
     DefaultTableModel model = new DefaultTableModel();
    // MedPanel medPanel;



     public PillBoxFrame(List<MedicationPlan> meds){

         this.add(topPanel, BorderLayout.NORTH);
         topPanel.add(title, BorderLayout.CENTER);
         title.setFont(new Font("Georgia",0, 24));

    //Timer
         timer.setFont(new Font("Georgia", 2, 24));
         topPanel.add(timer, BorderLayout.EAST);


         ScheduledThreadPoolExecutor timerChestie = new ScheduledThreadPoolExecutor(1);
         timerChestie.scheduleAtFixedRate(new Runnable() {
             @Override
             public void run() {
                 timer.setText((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))));
             }
         }, 0, 1, TimeUnit.SECONDS);

        this.add(nextPanel, BorderLayout.CENTER);
        nextPanel.setLayout(new BoxLayout(nextPanel, BoxLayout.PAGE_AXIS));


       for(MedicationPlan med: meds){
           MedPanel medPanel = new MedPanel(med);
           nextPanel.add(medPanel);
           nextPanel.add(Box.createRigidArea(new Dimension(0, 5)));
           if(!isOverdue(med)){
               medPanel.setBorder(BorderFactory.createLineBorder(Color.red, 5));
               notifyNotTaken(med);
           }
        }
        this.revalidate();
        this.repaint();

        this.setTitle("PillBox");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(1000, 600);

        this.setLocationRelativeTo(null);

    }

    private boolean isOverdue(MedicationPlan med){
         LocalTime now = LocalTime.now();
         if(now.isAfter(med.getStartHour())&&now.isBefore(med.getStartHour().plusHours(med.getIntakeInterval()))){
             return true;
         }
         return false;
    }

    private void notifyNotTaken(MedicationPlan med){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();
        PillDispenserServiceGrpc.PillDispenserServiceBlockingStub stub = PillDispenserServiceGrpc.newBlockingStub(channel);

        stub.notTaken(GrpcTaken.newBuilder()
                .setDrugId(1)
                .setMedPlanId(med.getId())
                .build());

        channel.shutdown();
    }

}
