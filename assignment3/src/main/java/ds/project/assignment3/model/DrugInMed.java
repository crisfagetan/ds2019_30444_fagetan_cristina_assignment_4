package ds.project.assignment3.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugInMed {

    private int drugId;
    private String name;
}
