package ds.project.assignment3.model;

import lombok.*;

import java.time.LocalTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlan {

    private Integer id;

    //TODO Make this list later
    private List<DrugInMed> drug;

    //The maximum number it can take in one day
    private Integer intakeCount;

    //once every <intakeInterval> hours
    private Integer intakeInterval;

    private LocalTime startHour;


    //-------------------------------------------------CONSTRUCTORS-----------------------------------------------------------------------


    //--------------------------------------------GET & SET-------------------------------------------------------------


}
