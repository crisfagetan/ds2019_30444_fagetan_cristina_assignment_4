package ds.project.assignment3.client;


import assign3.grpc.*;
import ds.project.assignment3.GUI.PillBoxFrame;
import ds.project.assignment3.model.DrugInMed;
import ds.project.assignment3.model.MedicationPlan;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class GrpcClient {

    public static LocalTime DOWNLOAD_TIME = LocalTime.of(11, 0,0,0);


    public static void main(String[] args) throws InterruptedException {


        GrpcMedPlan medPlanResponse = GrpcMedPlan.newBuilder().build();

        if(LocalTime.now().isAfter(DOWNLOAD_TIME))
        {
            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                    .usePlaintext()
                    .build();

            PillDispenserServiceGrpc.PillDispenserServiceBlockingStub stub = PillDispenserServiceGrpc.newBlockingStub(channel);

            medPlanResponse = stub.download(DownloadRequest.newBuilder()
                    .setPatientId(1)
                    .build());

            channel.shutdown();
        }
        System.out.println("Response received from server:\n" + medPlanResponse);

        List<MedicationPlan> medPlans = new ArrayList<>();

        for (GrpcMedication m: medPlanResponse.getMedicationList()) {
            List<DrugInMed> drugs = new ArrayList<>();
            for(GrpcDrug d: m.getDrugList()){
                DrugInMed dd = new DrugInMed();
                dd.setDrugId(d.getDrugId());
                dd.setName(d.getName());
                drugs.add(dd);
            }


            MedicationPlan medicationPlan = new MedicationPlan();
            medicationPlan.setId(m.getMedId());
            medicationPlan.setDrug(drugs);
            medicationPlan.setIntakeCount(m.getIntakeCount());
            medicationPlan.setIntakeInterval(m.getIntakeInterval());
            medicationPlan.setStartHour(LocalTime.parse(m.getStartHour()));

            medPlans.add(medicationPlan);
        }

        PillBoxFrame pillBoxFrame = new PillBoxFrame(medPlans);
        pillBoxFrame.setVisible(true);


    }
}
