import { Patient } from './patient';

export interface Caregiver {
    id: number;

    userId: number;

    firstName?: string;

    lastName?: string;

    dateOfBirth?: Date;

    gender?: boolean; // 1 = male; 0 = female

    address?: string;

    patients?: Patient[];
}
