export class Activity {
  id: number;

  patientId: number;

  startTime: Date;

  endTime: Date;

  activity: string;

  status: string;

  recommend: string;
}
