import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from '../_services/patient.service';
import { AlertifyService } from '../_services/alertify.service';
import { Activity } from '../_models/activity';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent implements OnInit {

  constructor(public formBuilder: FormBuilder, private patientService: PatientService,
              private alertify: AlertifyService, private route: ActivatedRoute, public router: Router) { }

              editForm: NgForm;

activity: Activity;

  ngOnInit() {
    this.loadActivity();
  }

  loadActivity() {
    // + is to be passed as a number instead of a string
      this.patientService.getActivity(+this.route.snapshot.paramMap.get('patientId'),
       +this.route.snapshot.paramMap.get('id'))
       .subscribe((activity: Activity) => {
        this.activity = activity;
      }, error => {
        this.alertify.error(error);
      });
    }

  recommendation() {
    this.patientService.changeActivities(this.activity).subscribe(next => {
      this.alertify.success('Activity updated successfully');
      this.router.navigateByUrl('/patient');
    }, error => {
      this.alertify.error(error);
    });


  }

}
