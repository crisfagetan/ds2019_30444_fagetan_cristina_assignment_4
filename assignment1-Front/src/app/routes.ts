import {Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DrugComponent } from './drug/drug.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { PatientComponent } from './patient/patient.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { DrugEditComponent } from './drug-edit/drug-edit.component';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { CaregiverEditComponent } from './caregiver-edit/caregiver-edit.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { MedPlanComponent } from './medPlan/medPlan.component';
import { MedplanEditComponent } from './medplan-edit/medplan-edit.component';
import { PatientGuard } from './_guards/patient.guard';
import { CaregiverGuard } from './_guards/caregiver.guard';
import { ActivityComponent } from './activity/activity.component';
import { RecommendationComponent } from './recommendation/recommendation.component';


export const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
// caregiver
    {path: 'caregiver', component: CaregiverComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'caregiver/patients/:id', component: PatientComponent,  runGuardsAndResolvers: 'always',
    canActivate: [PatientGuard]},
    {path: 'caregiver/patients/add/:id', component: PatientComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]}, // id is for caregiver - add patients for them
    {path: 'caregiver/:id', component: CaregiverEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'caregiver/insert', component: CaregiverEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},

// patient
    {path: 'patient', component: PatientComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'patient/activities/:id', component: ActivityComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'patient/:id', component: PatientEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'patient/insert', component: PatientEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'patient/recommendation/:patientId/:id', component: RecommendationComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},

// profile
    {path: 'profile/:id', component: ProfileComponent},

// drug
    {path: 'drug', component: DrugComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'drug/:id', component: DrugEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},
    {path: 'drug/insert', component: DrugEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]},

// MedPlan
    {path: 'patient/medPlan/:id', component: MedPlanComponent}, // id is for patient
    {path: 'medPlan/insert/:id', component: MedplanEditComponent, runGuardsAndResolvers: 'always',
    canActivate: [CaregiverGuard]}, // id is for patient

// default
    {path: '**', redirectTo: 'profile', pathMatch: 'full'}
];
