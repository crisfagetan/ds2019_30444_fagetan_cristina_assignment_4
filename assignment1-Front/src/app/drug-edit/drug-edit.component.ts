import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Drug } from '../_models/drug';
import { DrugService } from '../_services/drug.service';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-drug-edit',
  templateUrl: './drug-edit.component.html',
  styleUrls: ['./drug-edit.component.css']
})
export class DrugEditComponent implements OnInit {
  editForm: NgForm;
  drug: Drug;
  drugToInsert: any = {};
  isInsert: boolean;

  // @HostListener('window:beforeunload', ['$event'])
  // unloadNotification($event: any) {
  //   if (this.editForm.dirty) {
  //    $event.returnValue = true;
  //   }
  // }
  constructor(private drugService: DrugService, private alertify: AlertifyService,
              private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    if (this.router.url === '/drug/insert') {
      this.isInsert = true;
    } else {
      this.isInsert = false;
      this.loadDrug();
    }
  }

loadDrug() {
// + is to be passed as a number instead of a string
  this.drugService.getDrug(+this.route.snapshot.paramMap.get('id')).subscribe((drug: Drug) => {
    this.drug = drug;
  }, error => {
    this.alertify.error(error);
  });

  }

updateDrug() {
  this.drugService.updateDrug(this.drug).subscribe(next => {
    this.alertify.success('Drug updated successfully');
    this.router.navigateByUrl('/drug');
  }, error => {
    this.alertify.error(error);
  });
}

insertDrug() {
  this.drugService.insertDrug(this.drugToInsert).subscribe(next => {
    this.alertify.success('Drug inserted successfully');
    this.router.navigateByUrl('/drug');
  }, error => {
    this.alertify.error(error);
  });
}

}
