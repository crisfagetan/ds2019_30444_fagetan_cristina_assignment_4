import { Component, OnInit } from '@angular/core';
import { Activity } from '../_models/activity';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { PatientService } from '../_services/patient.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  activities: Activity[];
  public patientId: number;
  public isStatusOk: boolean;


  constructor(private patientService: PatientService,  private alertify: AlertifyService,
              public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.getPatientId();
    this.showActivities();
  }

  getPatientId() {
    this.patientId = +this.route.snapshot.paramMap.get('id');
    localStorage.setItem('patientId', this.patientId + '');
  }

  showActivities() {
    this.patientService.getActivities(+this.route.snapshot.paramMap.get('id')).subscribe((activities: Activity[]) => {
      this.activities = activities;
    }, error => {
      this.alertify.error(error);
    });
  }

  isOk(activity: Activity): boolean {
    if (activity.status === 'OK') {
      this.isStatusOk = true;
      return false;
    } else {
      this.isStatusOk = false;
      return true;
    }
  }

}
