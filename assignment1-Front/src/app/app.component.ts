import { Component } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { AlertifyService } from './_services/alertify.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular8-springboot-websocket';

  // webSocketEndPoint = 'http://localhost:8080/api/websocket';
  // topic = '/topic/event';
  // url = 'http://localhost:8080/topic/event';
  stompClient: any;
  constructor(public alertify: AlertifyService) {

  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    // this.initializeWebSocketConnection();
  }


  // initializeWebSocketConnection() {
  //   const ws = new SockJS(this.webSocketEndPoint);
  //   this.stompClient = Stomp.over(ws);
  //   const that = this;
  //   // tslint:disable-next-line: only-arrow-functions
  //   this.stompClient.connect({}, function(frame: any) {
  //     console.log('Connected to ' + frame);
  //     that.stompClient.subscribe(that.topic, (message: { body: string; }) => {
  //       if (message.body) {
  //         console.log(message.body);
  //         localStorage.setItem('message', message.body);
  //         if (localStorage.getItem('userRole') === '1') {
  //           that.alertify.warning(message.body + '');
  //           // that.alertify.success(localStorage.getItem('patientId'));
  //         }
  //       }
  //     });
  //   });
  // }
}
