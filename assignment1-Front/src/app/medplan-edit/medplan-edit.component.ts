import { Component, OnInit } from '@angular/core';
import { MedPlan } from '../_models/medPlan';
import { NgForm, FormBuilder, FormControl } from '@angular/forms';
import { MedPlanService } from '../_services/medPlan.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MedPlanAdd } from '../_models/medPlanAdd';
import { DrugService } from '../_services/drug.service';
import { Drug } from '../_models/drug';
import { DrugComponent } from '../drug/drug.component';

@Component({
  selector: 'app-medplan-edit',
  templateUrl: './medplan-edit.component.html',
  styleUrls: ['./medplan-edit.component.css']
})
export class MedplanEditComponent implements OnInit {
  medPlanAdd: MedPlanAdd;
  drugs: Drug[] = [];
  drugToAdd: Drug;

  constructor(public formBuilder: FormBuilder, private medPlanService: MedPlanService, private drugService: DrugService,
              private alertify: AlertifyService,
              private route: ActivatedRoute, public router: Router) { }

  registrationForm = this.formBuilder.group({
    drugsControl: new FormControl(''),
    intakeCount: new FormControl(''),
    intakeInterval: new FormControl(''),
    startHour: new FormControl(''),
    startDate: new FormControl(''),
    endDate: new FormControl('')
  });

  ngOnInit() {
    this.getDrugs();

  }

  getDrugs() {
    this.drugService.getDrugs().subscribe((drugs: Drug[]) => {
      this.drugs = drugs;
    }, error => {
      this.alertify.error(error);
    });
  }

  getDrug() {
    console.log('Value:' + +this.registrationForm.get('drugsControl').value);

    this.drugService.getDrug(this.registrationForm.get('drugsControl').value).subscribe((randName: Drug) => {
      console.log('Drug From method:' + randName.name);
      this.drugToAdd = randName;
      console.log('to Add:' + this.drugToAdd.name);
      // return drug;
    }, error => {
      this.alertify.error(error);

    });
    // return null;
  }

  insertMedPlan() {

    this.drugService.getDrug(this.registrationForm.get('drugsControl').value).subscribe((randName: any) => {
      // console.log('Drug From method:' + randName.name);
      // this.drugToAdd = randName;
      // console.log('to Add:' + this.drugToAdd.name);
      // return drug;
      // --------------------------------------------------------------------------------------
      this.medPlanAdd = new MedPlanAdd();
      this.medPlanAdd.intakeCount = +this.registrationForm.get('intakeCount').value;
      this.medPlanAdd.intakeInterval = +this.registrationForm.get('intakeInterval').value;
      this.medPlanAdd.startDate = this.registrationForm.get('startDate').value;
      this.medPlanAdd.startHour = this.registrationForm.get('startHour').value;
      this.medPlanAdd.endDate = this.registrationForm.get('endDate').value;
      // this.getDrug();
      this.medPlanAdd.drugs = [];
      this.medPlanAdd.drugs.push(randName);
      console.log(this.medPlanAdd);
      this.medPlanService.insertMedPlan(this.medPlanAdd).subscribe(next => {
        this.alertify.success('MedPlan inserted successfully');
        this.router.navigateByUrl('/patient');
      }, error => {
        this.alertify.error(error);
      });
      // ----------------------------------------------------------------
    }, error => {
      this.alertify.error(error);
    });
  }


}
