import { Component, OnInit } from '@angular/core';
import { UserLogin } from '../_models/userLogin';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userForLogin: UserLogin;
  user: User;
  model: any = {};

  constructor(private authService: AuthService, private alertify: AlertifyService,
              private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe((user: User) => {
      console.log(user);
      localStorage.setItem('userId', JSON.stringify(user.id));
      localStorage.setItem('userRole', JSON.stringify(user.role));
      localStorage.setItem('userRoleId', JSON.stringify(user.userInRoleId));
      localStorage.setItem('out', '0');
      this.alertify.success('Logged in successfully');
    }
    , error => {
      this.alertify.error(error);
    }, () => {
      this.router.navigate(['/profile/' + localStorage.getItem('userId')]);
    });
  }

}
