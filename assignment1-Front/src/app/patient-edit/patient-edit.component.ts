import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Patient } from '../_models/patient';
import { PatientService } from '../_services/patient.service';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.css']
})
export class PatientEditComponent implements OnInit {
  editForm: NgForm;
  patient: Patient;
  drugToInsert: any = {};
  isInsert: boolean;

constructor(private patientService: PatientService, private alertify: AlertifyService,
            private route: ActivatedRoute, public router: Router) { }


  ngOnInit() {
     this.loadPatient();
  }

  loadPatient() {
  // + is to be passed as a number instead of a string
    this.patientService.getPatient(+this.route.snapshot.paramMap.get('id')).subscribe((patient: Patient) => {
      this.patient = patient;
    }, error => {
      this.alertify.error(error);
    });
}

  updatePatient() {
    this.patientService.updatePatient(this.patient).subscribe(next => {
      this.alertify.success('Patient updated successfully');
      this.router.navigateByUrl('/patient');
    }, error => {
      this.alertify.error(error);
    });
  }

}
