package ds.project.assignment14.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.project.assignment14.entities.Doctor;
import ds.project.assignment14.entities.Drug;
import ds.project.assignment14.entities.MedicationPlan;
import ds.project.assignment14.entities.Patient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedPlanToAddDTO {


    private Integer id;

    List<Drug> drugs;

    private Integer intakeCount;

    private Integer intakeInterval;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="HH:mm")
    private LocalTime startHour;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate endDate;

    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static MedPlanToAddDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedPlanToAddDTO(
                medicationPlan.getId(),
//                medicationPlan.getDrugs().stream()
//                        .map(DrugDTO::generateDTOFromEntity)
//                        .collect(Collectors.toList()),
                medicationPlan.getDrugs(),
                medicationPlan.getIntakeCount(),
                medicationPlan.getIntakeInterval(),
                medicationPlan.getStartHour(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate());
    }

    public static MedicationPlan generateEntityFromDTO(MedPlanToAddDTO medPlanToAddDTO, Doctor doctor, Patient patient){
        return new MedicationPlan(
                medPlanToAddDTO.getId(),
                doctor,
                patient,
//                medPlanToAddDTO.getDrugs().stream()
//                        .map(DrugDTO::generateEntityFromDTO)
//                        .collect(Collectors.toList()),
                medPlanToAddDTO.getDrugs(),
                medPlanToAddDTO.getIntakeCount(),
                medPlanToAddDTO.getIntakeInterval(),
                medPlanToAddDTO.getStartHour(),
                medPlanToAddDTO.getStartDate(),
                medPlanToAddDTO.getEndDate(),
                false,
                false);
    }

}
