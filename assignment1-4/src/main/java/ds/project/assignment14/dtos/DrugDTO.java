package ds.project.assignment14.dtos;

import ds.project.assignment14.entities.Drug;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugDTO {

    private Integer id;
    private String name;
    private String sideEffects;
    private Integer dosage;


    //-------------------------------------------- MAPPERS -------------------------------------------------------------
    public static DrugDTO generateDTOFromEntity(Drug drug){
        return new DrugDTO(
                drug.getId(),
                drug.getName(),
                drug.getSideEffects(),
                drug.getDosage());
    }

    public static Drug generateEntityFromDTO(DrugDTO drugDTO){
        return new Drug(
                drugDTO.getId(),
                drugDTO.getName(),
                drugDTO.getSideEffects(),
                drugDTO.getDosage());
    }

}
