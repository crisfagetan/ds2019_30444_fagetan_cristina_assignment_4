package ds.project.assignment14.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.project.assignment14.entities.Doctor;
import ds.project.assignment14.entities.Drug;
import ds.project.assignment14.entities.MedicationPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedPlanViewDTO {

    private Integer id;

    private Doctor doctor;

 //   private Patient patient;

    List<Drug> drugs;

    private Integer intakeCount;

    private Integer intakeInterval;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="HH:mm")
    private LocalTime startHour;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate endDate;


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static MedPlanViewDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedPlanViewDTO(
                medicationPlan.getId(),
                medicationPlan.getDoctor(),
             //   medicationPlan.getPatient(),
                medicationPlan.getDrugs(),
                medicationPlan.getIntakeCount(),
                medicationPlan.getIntakeInterval(),
                medicationPlan.getStartHour(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate());
    }

    public static MedicationPlan generateEntityFromDTO(MedPlanViewDTO forViewDTO){

        return new MedicationPlan(
                forViewDTO.getId(),
                forViewDTO.getDoctor(),
            //    forViewDTO.getPatient(),
                forViewDTO.getDrugs(),
                forViewDTO.getIntakeCount(),
                forViewDTO.getIntakeInterval(),
                forViewDTO.getStartHour(),
                forViewDTO.getStartDate(),
                forViewDTO.getEndDate());
    }
}
