package ds.project.assignment14.dtos;

import ds.project.assignment14.entities.Doctor;
import ds.project.assignment14.entities.MedicationPlan;
import ds.project.assignment14.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorViewDTO {

    private Integer id;

    private  Integer userId;

    private String firstName;

    private String lastName;

    private Date dateOfBirth;

    private Boolean gender; //1 = male; 0 = female

    private String address;

    private List<MedicationPlan> medPlans;


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(
                doctor.getId(),
                doctor.getDoctorUser().getId(),
                doctor.getDoctorUser().getFirstName(),
                doctor.getDoctorUser().getLastName(),
                doctor.getDoctorUser().getDateOfBirth(),
                doctor.getDoctorUser().getGender(),
                doctor.getDoctorUser().getAddress(),
                doctor.getMedPlans());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO forViewDTO, User user){
        user.setAddress(forViewDTO.getAddress());
        user.setFirstName(forViewDTO.getFirstName());
        user.setLastName(forViewDTO.getLastName());

        return new Doctor(
                forViewDTO.getId(),
                user,
                forViewDTO.getMedPlans(),
                false);
    }
}
