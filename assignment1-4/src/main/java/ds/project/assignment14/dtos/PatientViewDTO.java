package ds.project.assignment14.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.project.assignment14.entities.MedicationPlan;
import ds.project.assignment14.entities.Patient;
import ds.project.assignment14.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientViewDTO {

    private Integer id;

    private Integer userId;

    private String firstName;

    private String lastName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date dateOfBirth;

    private Boolean gender; //1 = male; 0 = female

    private String address;

    private String medicalRecord;

    private List<MedicationPlan> medPlans;


    //---------------------------------------------- MAPPERS -----------------------------------------------------------
    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getId(),
                patient.getPatientUser().getId(),
                patient.getPatientUser().getFirstName(),
                patient.getPatientUser().getLastName(),
                patient.getPatientUser().getDateOfBirth(),
                patient.getPatientUser().getGender(),
                patient.getPatientUser().getAddress(),
                patient.getMedicalRecord(),
                patient.getMedPlans());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO forViewDTO, User user){
        user.setAddress(forViewDTO.getAddress());
        user.setFirstName(forViewDTO.getFirstName());
        user.setLastName(forViewDTO.getLastName());
        return new Patient(
                forViewDTO.getId(),
                forViewDTO.getMedicalRecord(),
                user,
                forViewDTO.getMedPlans());
    }
}
