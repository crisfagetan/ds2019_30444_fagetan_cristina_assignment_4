package ds.project.assignment14.controllers;

import ds.project.assignment14.dtos.CaregiverViewDTO;
import ds.project.assignment14.dtos.PatientViewDTO;
import ds.project.assignment14.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public List<CaregiverViewDTO> findAll(){
        return caregiverService.findAll();
    }

    //Dupa Id de la user
    @GetMapping(value = "/{id}")
    public CaregiverViewDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findUserById(id);
    }

    @GetMapping(value = "/{id}/patients")
    public List<PatientViewDTO> seePatients(@PathVariable("id") Integer id){
        return caregiverService.getCaregiverPatients(id);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody CaregiverViewDTO caregiverViewDTO) { return caregiverService.update(caregiverViewDTO); }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody CaregiverViewDTO caregiverViewDTO){
        caregiverService.delete(caregiverViewDTO);
    }

}
