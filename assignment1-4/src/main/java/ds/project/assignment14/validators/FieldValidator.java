package ds.project.assignment14.validators;

public interface FieldValidator<T> {
    boolean validate(T t);
}
