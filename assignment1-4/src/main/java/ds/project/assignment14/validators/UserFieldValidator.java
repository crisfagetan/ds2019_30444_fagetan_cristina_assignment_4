package ds.project.assignment14.validators;

import ds.project.assignment14.dtos.UserForRegisterDTO;
import ds.project.assignment14.errorhandlers.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class UserFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(UserFieldValidator.class);


    public static void validateRegister(UserForRegisterDTO userForRegisterDTO) {

        List<String> errors = new ArrayList<>();
        if (userForRegisterDTO == null || userForRegisterDTO.getRole()>2 || userForRegisterDTO.getRole()<0) {
            errors.add("userForRegisterDTO is null");
            throw new IncorrectParameterException(UserForRegisterDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(UserFieldValidator.class.getSimpleName(), errors);
        }
    }


}
