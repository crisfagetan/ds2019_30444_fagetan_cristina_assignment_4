package ds.project.assignment14.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patients")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Patient{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "med_record")
    private String medicalRecord;

    @JsonIgnore
    @JoinColumn(name = "user_id")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User patientUser;

    @JsonIgnore
    @ManyToMany(mappedBy = "patients", fetch = FetchType.LAZY)
    private List<Caregiver> caregivers;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MedicationPlan> medPlans;

    @Column(name = "is_removed")
    private Boolean isRemoved;

    //------------------------------------------------- CONSTRUCTORS ----------------------------------------------------------------------

    public Patient(Integer id, String medicalRecord, User patientUser, List<MedicationPlan> medPlans) {
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.patientUser = patientUser;
        this.medPlans = medPlans;
        this.isRemoved = false;
    }

    public Patient(User user){
        this.patientUser = user;
        this.id = 0;
        this.caregivers = new ArrayList<Caregiver>();
        this.medicalRecord = "";
        this.medPlans = new ArrayList<MedicationPlan>();
        this.isRemoved = false;
    }


    //------------------------------------------------- GET & SET ----------------------------------------------------------------------


}
