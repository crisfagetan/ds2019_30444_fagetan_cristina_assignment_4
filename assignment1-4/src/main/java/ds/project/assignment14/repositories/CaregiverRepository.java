package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.Caregiver;
import ds.project.assignment14.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

     public Caregiver findCaregiverByCaregiverUser(User userId);
     public Caregiver findCaregiverByIdAndIsRemoved(int id, Boolean isRemoved);
     public List<Caregiver> findAllByIsRemoved(Boolean b);



}

