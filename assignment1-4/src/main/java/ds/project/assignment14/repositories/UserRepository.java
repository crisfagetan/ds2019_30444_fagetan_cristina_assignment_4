package ds.project.assignment14.repositories;

import ds.project.assignment14.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

//    @Query(value = "SELECT new ds.project.assignment14.dtos.UserForViewDTO(u.id, u.username, u.role) " +
//            "FROM User u " +
//            "WHERE u.username = username AND u.password = password")
//    UserForViewDTO login(String username, String password);

    public List<User> findAllByIsRemoved(Boolean isRemoved);
    public User findUserByIdAndIsRemoved(int id, Boolean isRemoved);
    public User findUserByUsernameAndPassword(String username, String password);
}
