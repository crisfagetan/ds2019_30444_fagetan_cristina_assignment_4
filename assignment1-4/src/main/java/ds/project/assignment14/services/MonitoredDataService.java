package ds.project.assignment14.services;

import ds.project.assignment14.entities.MonitoredData;
import ds.project.assignment14.repositories.MonitoredDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MonitoredDataService {

    private final MonitoredDataRepository monitoredDataRepository;

    @Autowired
    public MonitoredDataService(MonitoredDataRepository monitoredDataRepository) {
        this.monitoredDataRepository = monitoredDataRepository;
    }

    //-------------------------------------------- FIND ----------------------------------------------------------------

    public List<MonitoredData> findAll(){
        List<MonitoredData> m = monitoredDataRepository.findAll();

        return m;
    }

//    public DrugDTO findDrugById(Integer id){
//        Drug drug  = monitoredDataRepository.findDrugByIdAndIsRemoved(id, false);
//
//        if (drug == null) {
//            throw new ResourceNotFoundException("Drug", "user id", id);
//        }
//        return DrugDTO.generateDTOFromEntity(drug);
//    }
//
//    public Drug findDrugById2(Integer id){
//        Drug drug  = monitoredDataRepository.findDrugByIdAndIsRemoved(id, false);
//
//        if (drug == null) {
//            throw new ResourceNotFoundException("Drug", "user id", id);
//        }
//        return drug;
//    }

    //-------------------------------------------- CREATE ----------------------------------------------------------------

    public Integer insert(MonitoredData monitoredData) {

        return monitoredDataRepository
                .save(monitoredData)
                .getId();
    }







}
