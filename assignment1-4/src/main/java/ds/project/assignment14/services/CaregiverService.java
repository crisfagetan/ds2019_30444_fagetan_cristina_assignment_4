package ds.project.assignment14.services;


import ds.project.assignment14.dtos.CaregiverViewDTO;
import ds.project.assignment14.dtos.PatientViewDTO;
import ds.project.assignment14.entities.Caregiver;
import ds.project.assignment14.entities.Patient;
import ds.project.assignment14.errorhandlers.ResourceNotFoundException;
import ds.project.assignment14.repositories.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    //------------------------------------------ FIND -------------------------------------------------------------------

    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAllByIsRemoved(false);

        return caregivers.stream()
                .map(CaregiverViewDTO::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public CaregiverViewDTO findUserById(Integer id){
        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(id, false);

        if (caregiver == null) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
        }

        return CaregiverViewDTO.generateDTOFromEntity(caregiver);
    }

    //see all Patients for Caregiver
    public List<PatientViewDTO> getCaregiverPatients(Integer caregiverId){
        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(caregiverId, false);

        if (caregiver == null) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverId);
        }

        List<PatientViewDTO> patients = new ArrayList<PatientViewDTO>();
        for (Patient p: caregiver.getPatients()) {
            patients.add(PatientViewDTO.generateDTOFromEntity(p));
        }
        return patients;
    }

    //see all Patients for Caregiver
    public List<PatientViewDTO> getCaregiverNonPatients(Integer caregiverId){
        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(caregiverId, false);

        if (caregiver == null) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverId);
        }

        List<PatientViewDTO> patients = new ArrayList<PatientViewDTO>();
        for (Patient p: caregiver.getPatients()) {
            patients.add(PatientViewDTO.generateDTOFromEntity(p));
        }

//        List<PatientViewDTO> allPatients = patientR

        return patients;
    }

    //------------------------------------------------ CREATE ----------------------------------------------------------
    public Integer insertCaregiver(Caregiver caregiver){
        try{
            return caregiverRepository
                    .save(caregiver)
                    .getId();
        }
        catch (Exception e){
            throw e;
        }

    }


    //------------------------------------------------- UPDATE ---------------------------------------------------------
    public Integer update(CaregiverViewDTO caregiverViewDTO) {

        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(caregiverViewDTO.getId(), false);

        if(caregiver == null){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverViewDTO.getId().toString());
        }
        //TODO
       // UserFieldValidator.validateRegister(caregiverViewDTO);


        Caregiver caregiverToUpdate = CaregiverViewDTO.generateEntityFromDTO(caregiverViewDTO, caregiver.getCaregiverUser());

        try{
            return caregiverRepository.save(caregiverToUpdate).getId();
        }
        catch(Exception e){
            throw  e;
        }
    }



    //------------------------------------------------ DELETE ----------------------------------------------------------


    public void delete(CaregiverViewDTO caregiverViewDTO){

        Caregiver caregiver = caregiverRepository.findCaregiverByIdAndIsRemoved(caregiverViewDTO.getId(), false);
        caregiver.setIsRemoved(true);
        caregiver.getCaregiverUser().setIsRemoved(true);
       // caregiver.getPatients().forEach(patient -> patient.getCaregivers().);
        caregiver.getPatients().clear();
        caregiverRepository.save(caregiver);
    }




}
